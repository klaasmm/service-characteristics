# Self Description Attributes - Overview

Version: 24/05/2022 22:47:06


## Outline

- [BigData](#bigdata)
- [Compute](#compute)
- [DataConnector](#dataconnector)
- [DataResource](#dataresource)
- [Database](#database)
- [DigitalIdentifyWallet](#digitalidentifywallet)
- [IdentityAccessManagement](#identityaccessmanagement)
- [IdentityFederation](#identityfederation)
- [IdentityProvider](#identityprovider)
- [Infrastructure](#infrastructure)
- [InstantiatedVirtualResource](#instantiatedvirtualresource)
- [LegalPerson](#legalperson)
- [Network](#network)
- [Node](#node)
- [Orchestration](#orchestration)
- [Participant](#participant)
- [PhysicalNode](#physicalnode)
- [PhysicalResource](#physicalresource)
- [Platform](#platform)
- [Resource](#resource)
- [ServiceOffering](#serviceoffering)
- [Software](#software)
- [Standard](#standard)
- [Storage](#storage)
- [TrustedCloudProvider](#trustedcloudprovider)
- [TrustedCloudServiceOffering](#trustedcloudserviceoffering)
- [TrustedCloudServiceOfferingArchitecture](#trustedcloudserviceofferingarchitecture)
- [TrustedCloudServiceOfferingCertificates](#trustedcloudserviceofferingcertificates)
- [TrustedCloudServiceOfferingContracts](#trustedcloudserviceofferingcontracts)
- [TrustedCloudServiceOfferingDataProtection](#trustedcloudserviceofferingdataprotection)
- [TrustedCloudServiceOfferingInteroperability](#trustedcloudserviceofferinginteroperability)
- [TrustedCloudServiceOfferingOperativeProcesses](#trustedcloudserviceofferingoperativeprocesses)
- [TrustedCloudServiceOfferingSecurity](#trustedcloudserviceofferingsecurity)
- [TrustedCloudServiceOfferingSubCompanies](#trustedcloudserviceofferingsubcompanies)
- [VerifiableCredentialWallet](#verifiablecredentialwallet)
- [VirtualNode](#virtualnode)
- [VirtualResource](#virtualresource)
- [Wallet](#wallet)


# BigData
# Service Offering - Compute

```mermaid
classDiagram

Platform <|-- BigData 
```

 **Super class**: [Platform](#platform)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# Compute
# Service Offering - Compute

```mermaid
classDiagram

Service Offering <|-- Compute 
```

 **Super class**: [Infrastructure](#infrastructure)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | type | xsd:string |  0 |  unlimited | Type of this compute Service Offering limited to virtual, bare metal or mixed | virtual, bare metal, mixed | 



# DataConnector
```mermaid
classDiagram

ServiceOffering <|-- Software
Software <|--  DataServiceOffering
DataServiceOffering --> "1..*" DataResource

```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | physicalResource | gax-resource:PhysicalResource | 1 |  unlimited | a list of resource with information of where is the data located | https://gaia-x.eu | 
| virtualResource | gax-resource:VirtualResource | 1 |  unlimited | a list of resource with information of who owns the data. | https://gaia-x.eu | 
| policies | xsd:anyURI | 1 |  unlimited | a list of policy expressed using a DSL used by the data connector policy engine leveraging Gaia-X Self-descriptions as both data inputs and policy inputs | https://gaia-x.eu | 
| type | xsd:string |  0 |  unlimited | Type of the data asset, which helps discovery.  Preferably a controlled vocabulary entry referenced by URI | dataset, container | 
| creationTime | xsd:dateTimeStamp |  0 |  unlimited | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| standardConformity | gax-core:Standard |  0 |  unlimited | Provides information about applied standards. | (reference to standard | 
| endpoint | gax-service:Endpoint |  0 |  unlimited | Endpoint through which the DataServiceOffering can be accessed | (reference to endpoint) | 



# DataResource
```mermaid
classDiagram

ServiceOffering <|--   DataExchangeConnector

```

 **Super class**: [VirtualResource](#virtualresource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | tags | xsd:string |  0 |  unlimited | Tags and Keywords describing this data resource. | air quality, time series data, measurements | 
| creationTime | xsd:dateTimeStamp |  0 |  unlimited | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| modificationDate | xsd:dateTimeStamp |  0 |  unlimited | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| updateInterval | xsd:string |  0 |  unlimited | Interval in which new data points are added to the data set. | P1D | 
| geographicCoverage | xsd:string |  0 |  unlimited | Geographic region covered by the dataset. |  | 
| spatialResolution | xsd:string |  0 |  unlimited | The spatial resolution of the dataset. |  | 
| temporalCoverage | xsd:string |  0 |  unlimited | The time period covered by the data set. | 2021-07-17T00:31:30Z/2022-07-17T00:31:30Z | 
| temporalResolution | xsd:string |  0 |  unlimited | The time interval between data points, if this data set provides time series data | PT1S | 



# Database
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [Platform](#platform)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# DigitalIdentifyWallet
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [Wallet](#wallet)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# IdentityAccessManagement
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [Software](#software)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# IdentityFederation
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [IdentityAccessManagement](#identityaccessmanagement)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# IdentityProvider
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [IdentityAccessManagement](#identityaccessmanagement)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# Infrastructure
# Infrastructure

```mermaid
classDiagram

ServiceOffering <| -- Infrastructure
Infrastructure <|-- Compute
Infrastructure <|-- Storage
Infrastructure <|-- Network
```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# InstantiatedVirtualResource
```mermaid
classDiagram

class  InstantiatedVirtualResource
```

 **Super class**: [VirtualResource](#virtualresource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | maintainedBy | gax-participant:Participant | 1 |  unlimited | a list of participants  maintaining the resource in operational condition. | https://gaia-x.eu | 
| hostedOn | gax-resource:Resource | 1 | 1 | a resource where the instance of this virtual resource is being executed on. | https://gaia-x.eu | 
| tenantOwnedBy | gax-participant:Participant | 1 |  unlimited | a list of participant with contractual relation with the resource. | https://gaia-x.eu | 
| endpoint | vcard:Address | 1 |  unlimited | a list of exposed endpoints as defined in ISO/IEC TR 23188:2020 |   | 



# LegalPerson
```mermaid
classDiagram

class LegalPerson 
```

 **Super class**: [Participant](#participant)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | registrationNumber | xsd:string | 1 | 1 | Country’s registration number which identify one specific company. | ACME GmbH, Foo LLC | 
| legalAddress | vcard:Address | 1 | 1 | The full legal address of the organization. | (a structured object which has the attribute country as mandatory attribute and some other optional attributes e.g., the attributes vcard:street-address,vcard:locality and vcard:country-name) | 
| headquarterAddress | vcard:Address | 1 | 1 | The full legal address of the organization. | (a structured object which has the attribute country as mandatory attribute and some other optional attributes e.g., the attributes vcard:street-address,vcard:locality and vcard:country-name) | 
| leiCode | xsd:string | 0 | 1 | Unique LEI number as defined by https://www.gleif.org. | M07J9MTYHFCSVRBV2631 | 
| parentOrganisation | gax-participant:Participant | 0 |  unlimited | A list of direct participant that this entity is a subOrganization of, if any. | https://gaia-x.eu | 
| subOrganisation | gax-participant:Participant | 0 |  unlimited | A list of direct participant with an legal mandate on this entity, e.g., as a subsidiary. | https://gaia-x.eu | 



# Network
# Service Offering - Networking

```mermaid
classDiagram

Service Offering <|-- Networking 
```

 **Super class**: [Infrastructure](#infrastructure)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# Node
```mermaid
classDiagram

Node <|-- BareMetalNode
Node <|-- VirtualNode
Node <|-- Container
```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | CPU | gax-core:CPU |  0 |  unlimited | Description of CPU(s) of this node | a structured object of type cpu, e.g. CPU:numberOfCores=4, CPU:frequency:value=3.0 and CPU:frequency:measure:unit=GHz | 
| GPU | gax-core:GPU |  0 |  unlimited | Description of GPU(s) of this node. | a structured object of type gpu, e.g. GPU:memoryType=DDR6, GPU:memorySize:value=24 and GPU:memorySize:value:unit=GB | 
| ramsize | gax-core:Measure |  0 |  unlimited | Size of RAM of this node | a structured object of type measure, e.g. measure:value=950 and measure:unit=GB | 
| disk | gax-core:Disk |  0 |  unlimited | Description of disk(s) of this nodes | a structured object of type harddrive, e.g. harddrive:productid=6CX68AV, and harddrive:name=Xeon Platinum 8280,and harddrive:manufacture=NVIDA; harddrive:size:value=1000, harddrive:size:unit=GB, and harddrive:type=SSD | 
| NIC | gax-core:Measure |  0 |  unlimited | Description of network interface card(s) of this node | a structured object of type measure, e.g. measure:value=10 and measure:unit=GBase-T | 



# Orchestration
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [Platform](#platform)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | type | xsd:string | 0 | 1 | Type of this Orchestration Service Offering, such as kubernetes. | kubernetes | 



# Participant

```mermaid
classDiagram

Participant <|-- Provider
Participant <|-- Federator
Participant <|-- Consumer

Provider <|-- TrustedCloudProvider
```




 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# PhysicalNode
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# PhysicalResource
# Resource - PhysicalResource

```mermaid
classDiagram

Resource --o Resource
Resource <|-- PhysicalResource
PhysicalResource <|-- BareMetalNode

```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | maintainedBy | gax-participant:Participant | 1 |  unlimited | Participant maintaining the resource in operational condition and thus have physical access to it. | https://gaia-x.eu | 
| ownedBy | gax-participant:Participant | 0 |  unlimited | Participant owning the resource. | https://gaia-x.eu | 
| manufacturedBy | gax-participant:Participant | 0 |  unlimited | Participant manufacturing the resource. | https://gaia-x.eu | 
| locationAddress | xsd:string | 1 |  unlimited | A list of physical GPS in ISO 6709:2008/Cor 1:2009 format. | ''52° 31' 12.0288 N, 13° 24' 17.8344 E | 



# Platform
# Service Offering - Compute

```mermaid
classDiagram

```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# Resource

```mermaid
classDiagram

Resource o-- Resource
Provider "1" --> "*" Resource : operates
ServiceOffering o-- Resource
Resource <|-- DataResource
Resource <|-- SoftwareResource
Resource <|-- Node
Resource <|-- Interconnection
Resource <|-- NetworkingDevice

```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | aggregationOf | gax-resource:Resource | 0 |  unlimited | Id of Resources (self-description) related to the resource and that can exist independently of it. | https://gaia-x.eu | 
| name | xsd:string | 0 | 1 | Name of resource. | Example Resource | 
| description | xsd:string | 0 | 1 | A more detailed description of resource. | Example Resource placed somewhere in Europe | 



# ServiceOffering

```mermaid
classDiagram

Provider <-- ServiceOffering : "isOfferedBy"

ServiceOffering --> ServiceOffering : "dependsOn"
ServiceOffering o-- Resource : "isComposedBy"

ServiceOffering <|-- Software
ServiceOffering <|-- Platform
ServiceOffering <|-- Infrastructure
```




 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | providedBy | gax-participant:Participant | 1 | 1 | Id of Participant (self-descrription) providing this service offering. | https://gaia-x.eu | 
| aggregationOf | gax-resource:Resource | 0 |  unlimited | Id of Resource (self-descrription) related to the service and that can exist independently of it. | https://gaia-x.eu | 
| termsAndConditions | xsd:anyURI | 1 |  unlimited | a resolvable link to the Terms and Conditions appling to that service. | https://gaia-x.eu | 
| policies | xsd:string | 0 |  unlimited | a list of policy expressed using a DSL (e.g., Rego or ODRL) |   | 
| serviceTitle | xsd:string | 0 | 1 | Name of the service | Image classification ML service | 
| description | xsd:string | 0 | 1 | A description in natural language | An ML service for training, deploying and improving image classifiers. | 
| keyword | xsd:string | 0 |  unlimited | Keywords that describe / tag the service. | ML, Classification | 
| provisionType | xsd:string | 0 | 1 | Provision type of the service | Hybrid, gax:PrivateProvisioning | 
| dependsOn | gax-service:ServiceOffering |  0 |  unlimited | ID of Service Offering (self-description) related to the service and that can exist independently of it. | gax:Storage | 



# Software

```mermaid
classDiagram

ServiceOffering <|-- Software
Software --> "1" Node : hosted_on 
```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 


# Standard

```mermaid
classDiagram

    Standard <|-- DataAccessStandard
    Standard <|-- DataModelingStandard
    Standard <|-- Certification
    
```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | title | xsd:string | 1 | 1 | Name of the standard. | ISO10303-242:2014 | 
| standardReference | xsd:anyURI | 1 |  unlimited | Provides a link to schemas or details about applied standards. | https://www.iso.org/standard | 
| publisher | xsd:string | 0 | 1 | Publisher of the standard. | International Organization for Standardization | 



# Storage
# Service Offering - Storage

```mermaid
classDiagram

Service Offering <|-- Storage 
```

 **Super class**: [Infrastructure](#infrastructure)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | type | xsd:string | 0 | 1 | Type of this Storage Service Offering. Choose one of the following: object storage, block storage, file system | object storage, block storage | 



# TrustedCloudProvider
```mermaid
classDiagram

Provider <|-- TrustedCloudProvider
```

 **Super class**: [Participant](#participant)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | registrationDate | xsd:dateTimeStamp | 1 | 1 | Date of the legal registration. | 2021-10-18T12:00:00+01:00 | 
| SME | xsd:boolean | 1 | 1 | Is the company a "small or medium sized company". | yes, no | 
| mainAddress | vcard:Address | 1 | 1 | Address of the head office |  | 
| mainContact | vcard:Agent | 1 | 1 | Main contact of the organisation | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| dataProtectionContact | vcard:Agent | 1 | 1 | A contact within an organisation to contact for data protection purposes. | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| employeeCount | xsd:string | 1 | 1 | Employee count of the organisation. | 50, No specification | 
| employeeCountForCloudServices | xsd:string | 1 | 1 | Employee count of the organisation in the cloud service area. | 50, No specification | 
| salesVolume | xsd:string | 1 | 1 | Sales volume of the organisation. | 50.000.000, No specification | 
| category | xsd:enum | 1 | 1 | Specific category of the organisation. | Reseller, ISV, CSP, Others | 
| countPublicCloudServices | xsd:string | 1 | 1 | Publicly offered cloud services. |  | 
| experienceCloudServices | xsd:string | 1 | 1 | Experience / knowledge of the organisation in the cloud service business. | <1 year, 1-5 year, >5 year, Others | 
| auditableAfterUserRequest | xsd:boolean | 1 | 1 | Is it possible for the user to request an audit? | yes, no | 
| alreadyAudited | xsd:string | 1 | 1 | Was the organisation already audited? |  | 
| auditableRelatedToDataProtection | xsd:string | 1 | 1 | Experience / knowledge of the organisation in the cloud service business. | yes, no, No specification | 



# TrustedCloudServiceOffering

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingArchitecture
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingCertificate
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingContract
TrustedCloudServiceOffering <|-- TrustedCloudServiceDataProtection
TrustedCloudServiceOffering <|-- TrustedCloudServiceInteroperability
TrustedCloudServiceOffering <|-- TrustedCloudServiceOperativeProcesses
TrustedCloudServiceOffering <|-- TrustedCloudServiceSecurity
TrustedCloudServiceOffering <|-- TrustedCloudServiceSubCompanies


```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | serviceLogo | xsd:base64Binary |  0 |  unlimited | PDF / JPG / PNG file to show the logo of the service. | logo.png | 
| serviceModel | xsd:string |  0 |  unlimited | Service Model for the service | IaaS, PaaS | 
| website | xsd:anyURI |  0 |  unlimited | Web address for the service presentation. | http://myservice.com | 
| serviceShortDescription | xsd:string |  0 |  unlimited | A short description in natural language to display on the query site. | An ML service for training, deploying and improving image classifiers. | 



# TrustedCloudServiceOfferingArchitecture

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingArchitecture

```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | guaranteedTenantSeparation | xsd:string |  0 |  unlimited | Is a continuous tenant separation guaranteed? | No statement, Not guaranteed, Guaranteed, Guaranteed and can be proven | 
| tenantSeparation | xsd:string |  0 |  unlimited | How does the tenant separation work? | Freetext | 
| definitionOfScalingTypesOfTheInfrastructure | xsd:string |  0 |  unlimited | Are the scaling types and factors of the technical infrastructure defined? | No statement, No defined, Scaling types are defined, Scaling types are defined and assured by contract | 
| declarationsAboutTheScalingTypes | xsd:string |  0 |  unlimited | Has additional declarations about the scaling types | Freetext | 



# TrustedCloudServiceOfferingCertificates

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingCertificates
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | certificateName | xsd:string |  0 |  unlimited | Relevant certificate for the service | ISO XYZ | 
| certificateType | xsd:string |  0 |  unlimited | Type of the certificates | BSI IT-Grundschutz, ISO 27001, Trust in Cloud | 
| descriptionOfTestScope | xsd:string |  0 |  unlimited | Description of the scope of testing for this service | Freetext | 
| certificateAuthority | xsd:string |  0 |  unlimited | Certificate authority for this service or its certificate | TÜV Süd | 
| certificateDocument | xsd:string |  0 |  unlimited | Document that contains a certificate copy | certificates.pdf | 
| expirationDate | xsd:date |  0 |  unlimited | Date on which the certificate expires | 2122-12-21 | 
| regularAudits | xsd:boolean |  0 |  unlimited | Is the certificate regularly audited? | true, false | 



# TrustedCloudServiceOfferingContracts

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingContracts
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | preAvailableContractsForCustomers | xsd:string |  0 |  unlimited | Are the contracts available to the customer before the closing? | No statement, On request, Published | 
| serviceContractLink | xsd:string |  0 |  unlimited | Is there a link to the service contract? | Freetext | 
| applicableLaw | xsd:string |  0 |  unlimited | Is there applicable law? | No statement, German contract law, EU contract law | 
| allSubContractorsMentioned | xsd:string |  0 |  unlimited | Are all involved sub contractors mentioned? | No statement, On request, Yes | 
| allContractConditionsMeetBySubContractor | xsd:string |  0 |  unlimited | Are all contract conditions transferable to the sub contractors? | No statement, Sub contractors oblige, Sub contractors are bound by contract | 
| contractedAccessToCustomerDataAfterBankruptcyOrServiceInterruption | xsd:string |  0 |  unlimited | Is the access to customer data after the contract has ended, a bankruptcy occurred or other special cases, and is it bound by the contract? | No statement, No rules for data return, Contracted rules to return data | 
| contractedRulesToReturnData | xsd:string |  0 |  unlimited | Contracted rules to return customer data. | Freetext | 
| SLAInContract | xsd:string |  0 |  unlimited | Are there SLAs in the contract? | Yes, No, Maybe | 
| SLAObservanceBeCheckedByTheCustomer | xsd:boolean |  0 |  unlimited | Can the SLA observance be checked by the customer? | true, false | 
| SLADescription | xsd:string |  0 |  unlimited | Description of the SLAs | Freetext | 
| contractedLegalConsequencesForSLAViolation | xsd:boolean |  0 |  unlimited | Are there legal consequences for an SLA violation in the contract? | true, false | 
| continuousGuaranteeOfPrivacy | xsd:boolean |  0 |  unlimited | Is there a contracted continuous guarantee of privacy, integrity and resilience of the systems? | true, false | 
| contractedAvailabilityForDataAfterTechnicalProblems | xsd:boolean |  0 |  unlimited | Are there contracted availabilities for data after a physical or technical problem? | true, false | 
| pricingModelSpecifications | xsd:string |  0 |  unlimited | Are there specifications for the pricing model? | Freetext | 
| publishedPricingModel | xsd:boolean |  0 |  unlimited | Is the pricing model publicly available to all cutomers? | true, false | 
| pricingModelLink | xsd:string |  0 |  unlimited | Link to the pricing model | Freetext | 
| contractRuntimeSpecifications | xsd:string |  0 |  unlimited | Specifications for the contract runtimes | Freetext | 
| terminationSpecifications | xsd:string |  0 |  unlimited | Are there specifications for the contract termination and dates? | Freetext | 
| freeTrialPhase | xsd:boolean |  0 |  unlimited | Is there a free trial phase? | true, false | 
| sameDataProtectionAsCommercialService | xsd:boolean |  0 |  unlimited | Has the free trial phase the same data protection as a commercial service? | true, false | 
| contractChangesCommunicatedToCustomer | xsd:string |  0 |  unlimited | Are changes in the contract communicated to the customer beforehand? | No statement, All changes will be communicated, All changes will be communicated and the customer gets a special termination right | 
| rulesForChangeManagementInContract | xsd:string |  0 |  unlimited | Are rules for change management in the contract? | No statement, Not in the contract, In the contract | 
| descriptionOfChangeManagement | xsd:string |  0 |  unlimited | Description of change management rules | Freetext | 
| obligationToCooperateInContract | xsd:boolean |  0 |  unlimited | Are all obligations to cooperate or provisions defined in the contract? | true, false | 
| customerExemptionFromClaimsOfThirdParties | xsd:boolean |  0 |  unlimited | Is the customer exempt from all claims of third parties while using this service? | true, false | 
| contractedRulesForTerminationSupport | xsd:boolean |  0 |  unlimited | Are there rules defined in the contract for rule termination? | true, false | 
| descriptionForTerminationSupport | xsd:string |  0 |  unlimited | Description of the rules for termination support | Freetext | 



# TrustedCloudServiceOfferingDataProtection

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingDataProtection
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | goodImplementationOfDSGVOMeasures | xsd:string |  0 |  unlimited | How do appropriate measures of the DSGVO measures get implemented? | No statement, Formally described, According to a contract, Certified measurements | 
| detailsAboutDSGVOMeasures | xsd:string |  0 |  unlimited | Details about the implemented technical and organisational measurements | Freetext | 
| fulfillmentOfFormalRequirements | xsd:string |  0 |  unlimited | Are the predescribed formal requirements fulfilled? | No statement, Requirements are fulfilled, Measurements are documented in the contract, Measurements are documented publicly, Proof via certificate | 
| contractedSupportOfDFA | xsd:boolean |  0 |  unlimited | Has contracted support of a "data protection impact assessment" (DFA), if needed? | true, false | 
| contractedPseudonymizationOrEncryptionOfPersonalData | xsd:boolean |  0 |  unlimited | Is there contracted pseudonymization or encryption of personal data? | true, false | 
| contractedImplementationOfDataSubjectRights | xsd:boolean |  0 |  unlimited | Has contracted implementation of data subject rights? | true, false | 
| contractedDeletionOfDataAndLinksInTheCloud | xsd:boolean |  0 |  unlimited | Has a contracted deletion of files and there links in the cloud? | true, false | 
| typeOfPersonalDataAccordingToTheProvider | xsd:boolean |  0 |  unlimited | Are declarations in the ADV-contract about personal data as well as categories of the affected persons? | true, false | 
| proofsForTheCustomerAccordingToADVContract | xsd:string |  0 |  unlimited | Which proofs according to the ADV-contract can be given to the customer for examination? | Self-audit, Internal behaviour rules with external proof, Certificate about data protection, Approved behaviour rules, Certificates after Art. 42 DS-GVO, Freetext | 
| registryAboutCustomerProcessingActivities | xsd:boolean |  0 |  unlimited | Is a registry about the customers processing activities created according to Art. 30 EU-DSGVO? | true, false | 
| possibleLocationRestrictionForCustomerData | xsd:string |  0 |  unlimited | Can the hosting of customer data be restricted to a specific location? | No statement, No restriction, Restriction to a region, Restriction to the EU, Restriction to germany | 
| possibleLocationRestrictionForCustomerDataAdministration | xsd:string |  0 |  unlimited | Can the administration of customer data be restricted to a specific location? | No statement, No restriction, Restriction to a region, Restriction to the EU, Restriction to germany | 
| GEORegionOptions | xsd:string |  0 |  unlimited | Possible GEO-regions | Freetext | 
| guaranteeForImplementationOfDataSubjectRights | xsd:string |  0 |  unlimited | How can the requirements of the DSGVO according to data subect rights be fulfilled, e.g. the deletion of personal data? | Freetext | 
| guaranteeToDeleteLinksToCustomerData | xsd:string |  0 |  unlimited | How can it be guaranteed, that also links get deleted, if the data is removed? | Freetext | 
| contractedObligationForAllProcessingPersons | xsd:string |  0 |  unlimited | Is there a contracted obligation for all the people processing personal data according to Art. 28 Abs. 3 S. 2 lit. b, 29, 32 Abs. 4 DSG-VO? | Freetext | 
| dataProtectionCertificateName | xsd:string |  0 |  unlimited | Name of the data protection certificate | Freetext | 
| dataProtectionCertificateDetails | xsd:string |  0 |  unlimited | More details about the certificate e.g. examination, extension and so on | Freetext | 



# TrustedCloudServiceOfferingInteroperability

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingInteroperability
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | possibilityToAdministerServicesWithStandardizedAPI | xsd:string |  0 |  unlimited | Is there a possibility to administer services with a standardized API? | No statement, There are no APIs, Web-GUI, Documented APIs, Documented and standardized APIs | 
| standardizedFormatsForVMsAndContainers | xsd:string |  0 |  unlimited | Does the service use standardized formats virtual machines and containers? | No statement, Proprietary, Open format, Standardized format | 
| descriptionOfServiceStackStandards | xsd:string |  0 |  unlimited | Description of the standard, the service stack is built on. | Freetext | 
| userAlwaysAccessToCustomerData | xsd:string |  0 |  unlimited | Does the user always have access to customer data? | No statement, Export can be done after agreement, Export over documented API, Standardized APIs are offered | 
| descriptionAboutDataRepatriation | xsd:string |  0 |  unlimited | Is there a description about data repatriation and supported file formats? | Freetext | 
| standardizedAPIForServiceIntegration | xsd:string |  0 |  unlimited | Is there a standardized API to integrate the service into the customers IT-landscape? | No statement, No APIS are offered, Documented APIs, Standardized APIs | 
| descriptionAboutOfferedIntegrationAPIs | xsd:string |  0 |  unlimited | Description of the offered APIs to integrate the services | Freetext | 
| descriptionOfTechnicalRequirementsForServiceUsage | xsd:string |  0 |  unlimited | Description for the technical requirements to use the service | Freetext | 
| descriptionOfOrganizationalRequirementsForServiceUsage | xsd:string |  0 |  unlimited | Description for the organizational requirements to use the service | Freetext | 



# TrustedCloudServiceOfferingOperativeProcesses

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingOperativeProcesses
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | efficientServiceManagement | xsd:string |  0 |  unlimited | Is an efficient service management guaranteed (ITIL)? | No statement, Formally described, According to accepted procedure', Certified, Certified and checked regularly | 
| detailsAboutServiceManagement | xsd:string |  0 |  unlimited | Details about the service management | Freetext | 
| serviceManagementCertificateName | xsd:string |  0 |  unlimited | Service management certificate name | Freetext | 
| serviceManagementCertificateDetails | xsd:string |  0 |  unlimited | More details about the certificate e.g. examination, extension and so on | Freetext | 
| assuredServiceAvailability | xsd:string |  0 |  unlimited | Assured Service Availability in Percent / Year | No statement, 99.9%, 99.99%, 99.999% | 
| maximumDownTime | xsd:string |  0 |  unlimited | Maximum downtime of the service in hours | Freetext | 
| guaranteedFastRecoveryOfDataAndConnectionAfterEvent | xsd:string |  0 |  unlimited | Is a quick recovery of customer data or the connection to the service / data guaranteed after an interruption event? | Freetext | 
| descriptionOfBackupOptions | xsd:string |  0 |  unlimited | Description of backup options | Freetext | 
| guaranteeForDataProtectionOnBackup | xsd:string |  0 |  unlimited | Is it guaranteed, that data protection is also extended to the backup? | Freetext | 
| possibilityToProvisionServiceByCustomer | xsd:string |  0 |  unlimited | Can the customer provision the the service himself? | No statement, Customer cant provision, Customer can provision | 
| descriptionOfSupport | xsd:string |  0 |  unlimited | Description of the scope of support services | Freetext | 
| guaranteedResponseTimeOnNormalRequests | xsd:string |  0 |  unlimited | What is the guaranteed response time on normal requests? | No statement, 4 hours, 1 work day, <3 work days | 
| guaranteedResponseTimeOnCriticalRequests | xsd:string |  0 |  unlimited | What is the guaranteed response time on critical requests? | No statement, 4 hours, 1 work day, <3 work days | 
| averageTimeUntilResolvanceOfMinorProblems | xsd:string |  0 |  unlimited | Whats the average time until minor problems are resolved? | No statement, <1 work day, 1 work day, <4 work days | 
| averageTimeUntilResolvanceOfMajorProblems | xsd:string |  0 |  unlimited | Whats the average time until major problems are resolved? | No statement, <1 work day, 1 work day, <4 work days | 
| availabilityOfSupport | xsd:string |  0 |  unlimited | What is the availability of the support? | No statement, 5x8, 24/7 | 
| completeUserDocumentation | xsd:string |  0 |  unlimited | Is there a complete user documentation? | No statement, Complete and updated, Regularly updated, Updated version online | 
| completeSystemDocumentation | xsd:string |  0 |  unlimited | Is there a complete system documentation? | No statement, Complete and updated, On request | 
| offerForTraining | xsd:boolean |  0 |  unlimited | Is there an offer for training? | Yes, No | 
| specificationFromTrainingPartners | xsd:string |  0 |  unlimited | Specification from training partners | Freetext | 



# TrustedCloudServiceOfferingSecurity

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingSecurity
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | efficientManagementForInformationSafety | xsd:string |  0 |  unlimited | Is there an efficient management for information safety given, e.g. certification according to ISO 27001? | No statement, According to accepted procedure, Certified, Certified and checked regularly | 
| detailsAboutProcess | xsd:string |  0 |  unlimited | More details about the process in the IT safety management | Freetext | 
| descriptionOfTheProcessToReportDataLeaks | xsd:string |  0 |  unlimited | Description of the process to report data leaks to the customer | Freetext | 
| securityCertificateName | xsd:string |  0 |  unlimited | Certificate name for the security certificate | Freetext | 
| securityCertificateDetails | xsd:string |  0 |  unlimited | More details about the certificate e.g. examination, extension and so on | Freetext | 
| dataCenterCertificateName | xsd:string |  0 |  unlimited | Certificate name for the data center or infrastructure certificate | Freetext | 
| dataCenterCertificateDetails | xsd:string |  0 |  unlimited | More details about the certificate e.g. examination, extension and so on | Freetext | 
| encryptionTypesForDataTransmissionAndStorage | xsd:string |  0 |  unlimited | Encryption types that can be used to encrypt data for transmission and storage? | Freetext | 
| keyManagementOptions | xsd:string |  0 |  unlimited | Key management options | No statement, Key are set by the provider, Keys are set by the customer, Keys can be set by external services | 
| rightsAndRoleConcept | xsd:string |  0 |  unlimited | Which rights- and role-concept are used? | No statement, User-wide right- and role-concept, Others | 



# TrustedCloudServiceOfferingSubCompanies

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingSubCompanies
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | subCompanyName | xsd:string |  0 |  unlimited | Name of the sub company or owner of the data center | DataCenter Inc. | 
| legalFormOfSubCompany | xsd:string |  0 |  unlimited | Legal form of the sub company or owner of the data center | Inc. | 
| registerNumberOfSubCompany | xsd:string |  0 |  unlimited | Register number of the sub company or owner of the data center | Inc. | 
| mainAddressOfSubCompany | xsd:address |  0 |  unlimited | Address of the sub company or owner of the data center | Address | 
| dataCenterLocation | xsd:string |  0 |  unlimited | Location to at least federal level of the data center | Hessen, Germany | 
| independentSubCompany | xsd:string |  0 |  unlimited | Is the data center company internal or his legally sovereign company? | Company internal, Legally sovereign company | 
| subCompanyLogo | xsd:string |  0 |  unlimited | Logo of the sub company or data center | logo.png | 
| companyWebsite | xsd:string |  0 |  unlimited | Website of the sub company or data center | http://acme2.com | 
| numberOfEmployees | xsd:integer |  0 |  unlimited | Number of employees working for the company | 40 | 
| salesVolume | xsd:integer |  0 |  unlimited | Sales volume of the company | 30 | 
| companyCategory | xsd:string |  0 |  unlimited | Category of the company | Reseller, ISV, CSP, Others | 
| numberOfPublicCloudServices | xsd:string |  0 |  unlimited | Number of publicly offered cloud services | 12 | 
| experienceLevelForCloudServices | xsd:string |  0 |  unlimited | Experience level of this provider to provide cloud services. | No statement, <1 year, 1-5 years, >5 years | 



# VerifiableCredentialWallet
```mermaid
classDiagram

class VerifiableCredentialWallet
```

 **Super class**: [Wallet](#wallet)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | verifiableCredentialExportFormat | xsd:string | 1 |  unlimited | a list of machine readable format used to export verifiable credentials. | https://gaia-x.eu | 
| privateKeyExportFormat | xsd:string | 1 |  unlimited | a list of machine readable format used to export private keys. | https://gaia-x.eu | 



# VirtualNode
