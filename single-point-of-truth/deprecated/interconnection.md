```mermaid
classDiagram

Resource <|-- Interconnection

Interconnection --> "2" Networking Device : connects

Interconnection --> PhysicalMedium : consists_of
Interconnection --> Connection : consists_of
Interconnection --> Route : consists_of

```
