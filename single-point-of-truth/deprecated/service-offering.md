
```mermaid
classDiagram

Provider <-- ServiceOffering : "isOfferedBy"

ServiceOffering --> ServiceOffering : "dependsOn"
ServiceOffering o-- Resource : "isComposedBy"

ServiceOffering <|-- Software
ServiceOffering <|-- Platform
ServiceOffering <|-- Infrastructure
```



