```mermaid
classDiagram

class Resource{
    <<abstract>>
}

class Participant{
    <<abstract>>
}



ServiceOffering "1" ..> "1" Participant : providedBy
ServiceOffering o-- Resource: aggregationOf

Resource o-- Resource: aggregationOf

```