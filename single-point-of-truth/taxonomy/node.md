```mermaid
classDiagram

class Node {
    <<abstract>>
}

PhysicalResource <|-- PhysicalNode

Node <|-- PhysicalNode
Node <|-- VirtualNode

InstantiatedVirtualResource <|-- VirtualNode
```