```mermaid
classDiagram

Infrastructure <|-- Network

Network <|-- Interconnection
Network <|-- VirtualRouter
Network <|-- VirtualPrivateNetwork
Network <|-- DomainNameSystem
Network <|-- LoadBalancer
Network <|-- OverlayNetwork
Network <|-- InternetConnection
Network <|-- NetworkingSegment
Network <|-- Firewall
Network <|-- AntiDos
```