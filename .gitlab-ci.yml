workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DOCS_DIR: documentation

stages:
  - tests      # unit test
  - check      # check SPoT
  - build      # build SD Ontology artefacts
  - validate   # validate SD samples against SD Ontology 
  - doc        # generate documentaion
  - page       # generate landing page
   
#include:
#  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
#  - project: 'gaia-x/toolset/gaia-x-document-template'
#    ref: main # could also use git SHA
#    file: 'gitlab-ci-template.yml'


#########################################################
# unittests for toolchain aka CI/CD Pipeline
#########################################################
toolchain-unittests:
  # run python unit tests for tool chain
  # up-and-running ci/cd pipeline is pre-requiste for all other stages
  stage: tests
  image: python:3-buster
  before_script:
    - cd toolchain
    - pip install -Ur requirements.txt
    - pip install -Ur test-requirements.txt
  script:
    - python -m unittest
  only:
    changes:
      - toolchain/**/*


#########################################################
# check Single-Point-of-Truth stage
#########################################################
check-yaml-syntax:
  stage: check
  image: node:15-buster
  before_script:
    - npm install -g ajv-cli
  script:
    - >
      for filepath in $(find yaml -maxdepth 1 -type f -name "*.yaml"); do
        echo ${filepath}
        ajv test -s ./yaml/validation/schema.json -d $filepath --valid
      done

check-yaml-semantic:
  stage: check
  image: python:3-buster
  before_script:
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 check_yaml.py '../yaml' '../yaml/validation'
  #only:
  #  changes:
  #    - single-point-of-truth/**/*
  #    - yaml/**/*
  #    - conceptual-model/**/*
            

#########################################################
# build
#########################################################
create-shacl:
  stage: build
  image: python:3-buster
  before_script:
    - mkdir -p yaml2shacl/
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 yaml2shacl.py
    - python3 constraintIndex.py
  artifacts:
    paths:
      - yaml2shacl/

create-json:
  stage: build
  image: python:3-buster
  before_script:
    - mkdir -p yaml2json/
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 yaml2json.py
  artifacts:
    paths:
      - yaml2json/

create-ontology:
  stage: build
  image: python:3-buster
  before_script:
    - mkdir -p yaml2ontology/
    - cd toolchain
    - pip install -Ur requirements.txt
    - cd ontology_generation
  script:
    - python3 yaml2ttl.py
  artifacts:
    paths:
      - yaml2ontology/

#########################################################
# validate 
#########################################################
check-rdf-shacl:
  # validate SD instances in implementation/instances against 
  # shacl shapes build in create-shacl 
  stage: validate
  image: python:3-buster
  before_script:
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 check_shacl.py

#########################################################
# build-doc
#########################################################
create-UML:
  stage: doc
  image: ruby:2.7
  before_script:
    - rm -rf /var/lib/apt/lists/*
    - apt-get update
    - apt-cache gencaches
    - apt-get install -y default-jdk
    - apt-get install -y zip unzip
    - mkdir -p yaml2shacl/uml
    - apt install -y graphviz
    - wget https://github.com/sparna-git/shacl-play/releases/download/0.5/shacl-play-app-0.5-onejar.jar -O shacl-play.jar
  script:
    - echo "Create UML Visualization"
    - >
      for filename in yaml2shacl/*.ttl; do
        java -jar shacl-play.jar draw -i "$filename" -o "yaml2shacl/uml/$(basename "$filename" .ttl).png"
      done
  dependencies:
    - create-shacl
  artifacts:
    paths:
      - yaml2shacl/uml

visualize-sd-amples:
  stage: doc
  image: node:15-buster
  script:
    - cd toolchain/visualization
    - chmod +x build.sh
    - ./build.sh
  artifacts:
    paths:
      - toolchain/visualization/output/
  #only:
  #  - main

gen-attribute-tables:
  # checks if creation of sd attributes tables works
  stage: doc
  image: python:3-bullseye
  before_script:
    - pip install -Ur toolchain/requirements.txt
  script:
    - mkdir -p sd-attributes
    - python toolchain/docgen/cli.py sd-attributes --srcYaml yaml --srcConcept conceptual-model --fileName sd-attributes/sd-attributes.md
  artifacts:
    paths:
      - sd-attributes 
  #only:
  #  - main

gen-ontology-doc:
  stage: doc
  image: ruby:2.7
  before_script:
    - rm -rf /var/lib/apt/lists/*
    - apt-get update
    - apt-cache gencaches
    - apt-get install -y zip unzip
  script:
    - mkdir -p widoco/
    - touch widoco/index.html
    - mkdir -p widoco/provider/
    - cp implementation/instances/provider/*.ttl widoco/provider/ 2>/dev/null || true
    - cp implementation/instances/provider/*.jsonld widoco/provider/ 2>/dev/null || true
    - mkdir -p widoco/ontology/
    - mkdir -p widoco/core/
    - mkdir -p widoco/participant/
    - mkdir -p widoco/resource/
    - mkdir -p widoco/service/
    - mkdir -p widoco/constraints/
    - cp implementation/instances/service-offering/*.ttl widoco/service/ 2>/dev/null || true
    - cp implementation/instances/service-offering/*.jsonld widoco/service/ 2>/dev/null || true
    - cd widoco && find . -name \*.ttl -o -name \*.jsonld > files.txt
    - cd ..
    - echo "TTL and JSON-LD file provisioning done. Starting documentation..."

    - apt-get update
    - apt-get -y install default-jdk
    - echo "Creating documentation..."
    - wget https://github.com/dgarijo/Widoco/releases/download/v1.4.15_1/widoco-1.4.15-jar-with-dependencies.jar -O widoco.jar

    - echo "Creating ontology documentation..."
    - java -jar widoco.jar -ontFile toolchain/Ontology.ttl -outFolder docOntology -webVowl -uniteSections -rewriteAll
    - mv docOntology/index-en.html docOntology/ontology.html
    - cp -r docOntology/* widoco/ontology/
    - rm widoco/ontology/webvowl/data/ontology.json
    - java -jar ./toolchain/owl2vowl.jar -file widoco/ontology/ontology.xml
    - mv ontology.json widoco/ontology/webvowl/data/

    - echo "Creating core documentation..."
    - java -jar widoco.jar -ontFile yaml2ontology/core_generated.ttl -outFolder docCore -webVowl -uniteSections -rewriteAll
    - sed -i '/<tr><td><b>gax-core<\/b><\/td><td>&lt;http:\/\/w3id.org\/gaia-x\/core#&gt;<\/td><\/tr>/d' docCore/index-en.html
    - mv docCore/index-en.html docCore/core.html
    - cp -r docCore/* widoco/core/
    - rm widoco/core/webvowl/data/ontology.json
    - java -jar ./toolchain/owl2vowl.jar -file widoco/core/ontology.xml
    - mv ontology.json widoco/core/webvowl/data/

    - echo "Creating participant documentation..."
    - java -jar widoco.jar -ontFile yaml2ontology/participant_generated.ttl -outFolder docParticipant -webVowl -uniteSections -rewriteAll
    - sed -i '/<tr><td><b>gax-participant<\/b><\/td><td>&lt;http:\/\/w3id.org\/gaia-x\/participant#&gt;<\/td><\/tr>/d' docParticipant/index-en.html
    - mv docParticipant/index-en.html docParticipant/participant.html
    - cp -r docParticipant/* widoco/participant/
    - rm widoco/participant/webvowl/data/ontology.json
    - java -jar ./toolchain/owl2vowl.jar -file widoco/participant/ontology.xml
    - mv ontology.json widoco/participant/webvowl/data/

    - echo "Creating resource documentation..."
    - java -jar widoco.jar -ontFile yaml2ontology/resource_generated.ttl -outFolder docResource -webVowl -uniteSections -rewriteAll
    - sed -i '/<tr><td><b>gax-resource<\/b><\/td><td>&lt;http:\/\/w3id.org\/gaia-x\/resource#&gt;<\/td><\/tr>/d' docResource/index-en.html
    - mv docResource/index-en.html docResource/resource.html
    - cp -r docResource/* widoco/resource/
    - rm widoco/resource/webvowl/data/ontology.json
    - java -jar ./toolchain/owl2vowl.jar -file widoco/resource/ontology.xml
    - mv ontology.json widoco/resource/webvowl/data/

    - echo "Creating service documentation..."
    - java -jar widoco.jar -ontFile yaml2ontology/service_generated.ttl -outFolder docService -webVowl -uniteSections -rewriteAll
    - sed -i '/<tr><td><b>gax-service<\/b><\/td><td>&lt;http:\/\/w3id.org\/gaia-x\/service#&gt;<\/td><\/tr>/d' docService/index-en.html
    - mv docService/index-en.html docService/service.html
    - cp -r docService/* widoco/service/
    - rm widoco/service/webvowl/data/ontology.json
    - java -jar ./toolchain/owl2vowl.jar -file widoco/service/ontology.xml
    - mv ontology.json widoco/service/webvowl/data/


    - echo "Create JSONLD Visualization"
    - mkdir -p widoco/visualization/
    #- cp -r toolchain/visualization/output/* widoco/visualization/ # output does not exits
    - cp -r toolchain/visualization/* widoco/visualization/

    - mv toolchain/constraints.html widoco/

    - mkdir -p widoco/validation/
    - cp -r yaml2shacl/* widoco/validation

    - mkdir -p widoco/json-validation/
    - cp -r yaml2json/* widoco/json-validation/

    - mkdir -p widoco/ontology_autogen/
    - cp -r yaml2ontology/* widoco/ontology_autogen/
    
  artifacts:
    paths:
      - widoco 
  #only:
  #  - main
 
#########################################################
# generate landing page
#########################################################
# copied from gaia-x/toolset/gaia-x-document-template as include will break pipeline
generate-html:
  stage: page
  image: python:3-bullseye
  before_script:
    - git submodule update --recursive --remote
    - test -f requirements.txt && pip install -Ur requirements.txt
    - test -f gaia-x-document-template/requirements.txt && pip install -Ur gaia-x-document-template/requirements.txt
    - cp -r gaia-x-document-template/template_html/extra.css $DOCS_DIR/
    - cp -r gaia-x-document-template/template_html/extra.js $DOCS_DIR/
    - cp sd-attributes/sd-attributes.md $DOCS_DIR/
  script:
    - test -f generate_html_custom.sh && bash ./generate_html_custom.sh 
    - j2 mkdocs.yml.j2 > mkdocs.yml
    - cat mkdocs.yml
    - mkdocs build
    - test -f pre_publish_html.sh && bash pre_publish_html.sh
    - mkdir landing-page
    - cp -r public/* landing-page
    - echo "done" # If last command in script is 'test -f somefile', job fails
  artifacts:
    paths:
      - landing-page
    expire_in: 1 day


#########################################################
# build GitLab pages
#########################################################
pages:
  stage: .post
  script:
    - mkdir -p public/
    - mkdir -p public/yaml2shacl
    - mkdir -p public/yaml2json
    - mkdir -p public/yaml2ontology    
    - mkdir -p public/widoco   
    
    - cp -r landing-page/* public/
    - cp -r yaml2shacl/* public/yaml2shacl
    - cp -r yaml2json/* public/yaml2json
    - cp -r yaml2ontology* public/yaml2ontology
    - cp -r widoco/* public/widoco
  only:
    - main # possible solution for later with tags https://stackoverflow.com/questions/42796018/how-to-run-a-gitlab-ci-yml-job-only-on-a-tagged-branch/52807912#52807912
  artifacts:
    paths:
    - public # do not change it https://gitlab.com/gitlab-org/gitlab/-/issues/1719#note_30008931 ?
