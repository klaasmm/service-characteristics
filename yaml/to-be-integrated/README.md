# Yaml Files

Single Point of Truth of Self-Description Schema. [See Governance Process](../documentation/governance) for more details.

**Guidelines for writing valid YAML files:**

The following list serves as the ground truth for the validation script toolchain/check_yaml.py. <br />
The script is executed at every commit to any branch to validate the semantic correctness of all *.yaml files in this folder (also includes the validation folder).

**NOTE: Any changes to this list have to result in an update to the validation script (check_yaml.py)**

* Use one file per class in Conceptual model

* Yaml file name must start with lower case character. File name must be equal to name of class in Gaia-X Conceptual Model, e.g. `Provider.yaml` for class `Provider`. If name in Conceptual Model is a composed one, such as `Service Offering`, spaces are replace by dashes and all strings start with lower case character each. E.g.yaml file for `Service Offeriung` should be called `service-offering.yaml`.

* There has to be one same-named markdown file with conceptual model (mermaid) in folder [documentation/conceptual-model](/documentation/conceptual-model/)

* First element of each yaml file must be the name of a class in the Gaia-X Conceptual Model in upper CamelCase notion. E.g. Service Offering is translated to ServiceOffering.

* Key `subClassOf` is mandatory for a `class`. If `subClassOf` is set, value must be a list with super classes in upper CamelCase notation. For each list item in `subClassOf` an additional yaml file must exist. If class has no 'subClassOf' use an empty list. 

* Key `attributes` is mandatory for a `class`. It must be a list of at least one attribute. Each attribute has to consist of five mandatory keys (`title`,`prefix`,`dataType`,`descripton`,`exampleValues`). 

* Value of `title` is mandatory for an `attribute`. It must be unique within each file as it is used as ID of a Self-Description attribute.

* Value of `prefix` is mandatory for an `attribute`. It must be an abbreviation defined in [prefixes.yaml](validation/prefixes.yaml).  
It is needed to define the origin of the attribute to correctly generate subsequent files. 

* Value of `dataType` is mandatory for an `attribute`. It must be an abbreviation listed in [dataTypeAbbreviation.yaml](validation/dataTypeAbbreviation.yaml). Mapping of abbreviation in `dataTypeAbbreviation.yaml` must be a valid data type in Self-Description ontology.

* Key `cardinality` is optional for an `attribute`. If it is missing, the attribute is considered to be optional. If `cardinality` is set, key `minCount` is mandatory.

* Key `maxCount` is optional for `cardinality`. If it is missing, attribute is unlimited.

* Value of `minCount` and `maxCount` must be a positive integer

* Value of `descripton` is mandatory for an `attribute`. It must be at least one human readable sentence.

* Value of `exampleValues` is mandatory for an `attribute`. It must be a list. There must be at least one list item.

Note: Relationships between classes are modeled as (non-) mandatory attributes, too.

## Developer Support

The schema of the yaml files is formally described as JSON Schema in the 'schema.json' file. In the future this file might be publicly available, so it can be referenced directly.
This can be loaded into any IDE to get auto-completion.

* For VSCode: Install the 'YAML'-Extension. In your users settings add:

    ```text
        "yaml.schemas": {
            "./yaml/validation/schema.json": ["/yaml/*"]
        },
    ```

* For IntelliJ: Add the schema.json to the Schema Mappings

If you've installed Docker, you can run

```bash
docker run --rm -v ${PWD}:/yaml 3scale/ajv test -s /yaml/validation/schema.json -d /yaml/<YourYaml>.yaml  --valid
```

inside the yaml-folder to validate your file. To check all yaml files

```bash
 for f in ./*.yaml; do docker run --rm -v ${PWD}:/yaml 3scale/ajv test -s /yaml/validation/schema.json -d /yaml/$f --valid; done
```
