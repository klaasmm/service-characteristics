TrustedCloudServiceOfferingContracts:
    subClassOf: ['TrustedCloudServiceOffering']
    prefix: 'gax-service'
    attributes:
        # 5.1 General Contracts
        -
            title: 'preAvailableContractsForCustomers'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are the contracts available to the customer before the closing?'
            exampleValues: [ 'No statement', 'On request', 'Published' ]
        -
            title: 'serviceContractLink'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there a link to the service contract?'
            exampleValues: [ 'Freetext' ]
        -
            title: 'applicableLaw'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there applicable law?'
            exampleValues: [ 'No statement', 'German contract law', 'EU contract law' ]

        # 5.3 Transparency of sub contractors
        -
            title: 'allSubContractorsMentioned'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are all involved sub contractors mentioned?'
            exampleValues: [ 'No statement', 'On request', 'Yes' ]
        -
            title: 'allContractConditionsMeetBySubContractor'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are all contract conditions transferable to the sub contractors?'
            exampleValues: [ 'No statement', 'Sub contractors oblige', 'Sub contractors are bound by contract' ]

        # 5.4 Regulation on service interruption or bankruptcy
        -
            title: 'contractedAccessToCustomerDataAfterBankruptcyOrServiceInterruption'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is the access to customer data after the contract has ended, a bankruptcy occurred or other special cases, and is it bound by the contract?'
            exampleValues: [ 'No statement', 'No rules for data return', 'Contracted rules to return data' ]
        -
            title: 'contractedRulesToReturnData'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Contracted rules to return customer data.'
            exampleValues: [ 'Freetext' ]

        # 5.5 Service Level Agreements
        -
            title: 'SLAInContract'
            prefix: 'gax-service'
            dataType: 'xsd:string' # not the right type; see #295
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there SLAs in the contract?'
            # we need to handle this differently; see #295
            exampleValues: [ 'Yes', 'No', 'Maybe' ]
        -
            title: 'SLAObservanceBeCheckedByTheCustomer'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Can the SLA observance be checked by the customer?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'SLADescription'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Description of the SLAs'
            exampleValues: [ 'Freetext' ]
        -
            title: 'contractedLegalConsequencesForSLAViolation'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there legal consequences for an SLA violation in the contract?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'continuousGuaranteeOfPrivacy'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there a contracted continuous guarantee of privacy, integrity and resilience of the systems?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'contractedAvailabilityForDataAfterTechnicalProblems'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there contracted availabilities for data after a physical or technical problem?'
            exampleValues: [ 'true', 'false' ]

        # 5.6 Transparent pricing model
        -
            title: 'pricingModelSpecifications'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there specifications for the pricing model?'
            exampleValues: [ 'Freetext' ]
        -
            title: 'publishedPricingModel'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is the pricing model publicly available to all cutomers?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'pricingModelLink'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Link to the pricing model'
            exampleValues: [ 'Freetext' ]
        -
            title: 'contractRuntimeSpecifications'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Specifications for the contract runtimes'
            exampleValues: [ 'Freetext' ]
        -
            title: 'terminationSpecifications'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there specifications for the contract termination and dates?'
            exampleValues: [ 'Freetext' ]
        -
            title: 'freeTrialPhase'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there a free trial phase?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'sameDataProtectionAsCommercialService'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Has the free trial phase the same data protection as a commercial service?'
            exampleValues: [ 'true','false' ]

        # 5.7 Change management
        -
            title: 'contractChangesCommunicatedToCustomer'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are changes in the contract communicated to the customer beforehand?'
            exampleValues: [ 'No statement', 'All changes will be communicated', 'All changes will be communicated and the customer gets a special termination right' ]
        -
            title: 'rulesForChangeManagementInContract'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are rules for change management in the contract?'
            exampleValues: [ 'No statement', 'Not in the contract', 'In the contract' ]
        -
            title: 'descriptionOfChangeManagement'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Description of change management rules'
            exampleValues: [ 'Freetext' ]

        # 5.8
        -
            title: 'obligationToCooperateInContract'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are all obligations to cooperate or provisions defined in the contract?'
            exampleValues: [ 'true', 'false' ]

        # 5.9
        -
            title: 'customerExemptionFromClaimsOfThirdParties'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is the customer exempt from all claims of third parties while using this service?'
            exampleValues: [ 'true', 'false' ]
        # 5.10
        -
            title: 'contractedRulesForTerminationSupport'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are there rules defined in the contract for rule termination?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'descriptionForTerminationSupport'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Description of the rules for termination support'
            exampleValues: [ 'Freetext' ]
